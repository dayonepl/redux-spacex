import 'babel-polyfill'
import 'normalize.css'
import 'main.scss'

import React from 'react'
import ReactDOM from 'react-dom'
import {Provider} from 'react-redux'
import {
	Route,
	BrowserRouter as Router
} from 'react-router-dom'
import store from 'store'
import * as home from 'home'

const reducers = {

}

const reduxStore = store.initialize(reducers)

const Main = () => (
	<Provider store={reduxStore}>
		<Router>
			<Route exact path='/' component={home.component}/>
		</Router>
	</Provider>
)

ReactDOM.render(<Main />, document.getElementById('main'))
