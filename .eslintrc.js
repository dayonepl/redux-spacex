module.exports = {
    env: {
        browser: true,
        commonjs: true,
        es6: true,
    },
    extends: ['eslint:recommended', 'prettier'],
    parser: 'babel-eslint',
    parserOptions: {
        sourceType: 'module',
        ecmaFeatures: {
            experimentalObjectRestSpread: true,
            jsx: true,
        },
    },
    plugins: ['react'],
    rules: {
        'no-console': 'warn',
        'no-extra-boolean-cast': 'off',
        'react/jsx-uses-vars': 'error',
        'react/jsx-uses-react': 'error',
    },
}
